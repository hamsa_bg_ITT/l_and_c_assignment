#Assignment 2: The below program is to guess the correct number between 1 to 100
import random
def check_number_range(number):
    if number.isdigit() and 1<= int(number) <=100:
        return True
    return False

def read_number(message):
     user_guess=input(message)
     return user_guess
    

def check_guessed_number(number_to_guess):
    guessed_correctly=False
    number_of_guesses=0
    guessed_number=read_number("Guess a number between 1 and 100:")
    while not guessed_correctly:     
        if not check_number_range(guessed_number):
            guessed_number=read_number("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            number_of_guesses+=1
            guessed_number=int(guessed_number)
            if guessed_number<number_to_guess:
                guessed_number=read_number("Too low. Guess again")
            elif guessed_number>number_to_guess:
                guessed_number=read_number("Too High. Guess again")
            else:
                print("You guessed it in",number_of_guesses,"guesses!")
                guessed_correctly=True
                    

def main():
    number_to_guess=random.randint(1,100)
    check_guessed_number(number_to_guess)
    

main()

