# Formatting Document #
This document provides the rules for Formatting of code.

## Vertical Formatting

**1. Vertical openness between concepts**

Java and JS: Single line break between concepts for functions, import statements, etc...
Python: Two lines break.

**2. Function braces**

Java and JS: For functions, opening bracket at the end of the first line and one space before the opening bracket. Closing bracket on a new line, without leading spaces.

**3. Variable declaration**

All variables must be declared at the top of a module or functions where it will be going to use.

**4. Dependent Functions**

Java and JS: The caller should be above callee and should be vertically close.
Python: The caller should be below callee and should be vertically close.


## Horizontal Formatting

**1. Number of characters**

Java, JS, and Python: Line length must be limited to less than 100 characters.

**2. Horizontal openness and density**

Java, JS, and Python: There should not be white space between parenthesis after function name.

Java, JS, and Python: Higher precedence operators must not be separated by white spaces, whereas lower precedence operators must be separated by white spaces.

Java, JS, and Python: One whitespace after comma while declaring multiple variables in a single line and in function parameters.


## Indentation

Python and Java: 4 spaces should be used for indentation.

JS: 2 spaces should be used for indentation.

## Linter and Checkstyle Configuration:
**1. For Python**

Flake8

**2. For Java**

Language Support for Java(TM) by Red Hat - redhat.java


**3. For JavaScript**

ESlint
